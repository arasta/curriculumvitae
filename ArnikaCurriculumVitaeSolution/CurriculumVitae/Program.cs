﻿using CurriculumVitae.Domains;
using CurriculumVitae.Services;
using CurriculumVitae.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurriculumVitae.Domains.Enums;

namespace CurriculumVitae
{
    class Program
    {
        static void Main(string[] args)
        {
            var _dbContext = new CurriculumVitaeDbContext();

            IPersonService personService = new PersonService(_dbContext);
            IDrivingLicenseService drivingLicenseService = new DrivingLicenseService(_dbContext);
            IEducationService educationService = new EducationService(_dbContext);
            IJobExperienceService jobExperienceService = new JobExperienceService(_dbContext);
            ILanguageSkillService languageSkillService = new LanguageSkillService(_dbContext);
            ISertificateService sertificateService = new SertificateService(_dbContext);
            ISkillService skillService = new SkillService(_dbContext);
            IAnswerService answerService = new AnswerService(_dbContext);
            IQuestionService questionService = new QuestionService(_dbContext);

            Console.WriteLine("Filling database");


            if (personService.GetAllPersons().Count == 0)
            {
                personService.SavePerson(new Person() { Firstname = "Arnika", Lastname = "Rästa", Telephone = "+372 5247616", Email = "arnika.rasta@gmail.com", Gender = Gender.Naine, Address = "Harjumaa, Tallinn, Endla 74-2, 10613", BirthDate = new DateTime(1997, 09, 12) });
            }
            if (drivingLicenseService.GetAllDrivinglicenses().Count == 0)
            {
                drivingLicenseService.SaveDrivinglicense(new DrivingLicense() { PersonId = 1, DrivingLicenseCategory = DrivingLicenseCategory.B, DateAcquired = new DateTime(2015, 09, 12) });
                drivingLicenseService.SaveDrivinglicense(new DrivingLicense() { PersonId = 1, DrivingLicenseCategory = DrivingLicenseCategory.B1, DateAcquired = new DateTime(2015, 09, 12) });
            }
            if (educationService.GetAllEducations().Count == 0)
            {
                educationService.SaveEducation(new Education() { PersonId = 1, SchoolName = "Kaarepere põhikool", StartYear = 2004, EndYear = 2005, LevelOfEducation = LevelOfEducation.Põhiharidus, Graduated = false });
                educationService.SaveEducation(new Education() { PersonId = 1, SchoolName = "Oskar Lutsu Palamuse Gümnaasium", StartYear = 2005, EndYear = 2016, LevelOfEducation = LevelOfEducation.Keskharidus, Graduated = true });
                educationService.SaveEducation(new Education() { PersonId = 1, SchoolName = "Eesti Infotehnoloogia Kolledž", StartYear = 2016, LevelOfEducation = LevelOfEducation.Kõrgharidus, Graduated = false });
            }
            if (jobExperienceService.GetAllJobExperiences().Count == 0)
            {
                jobExperienceService.SaveJobExperience(new JobExperience() { PersonId = 1, Company = "OÜ Vudila Mängumaa", JobTitle = "Instruktor", StartDate = new DateTime(2013, 06, 01), EndDate = new DateTime(2013, 08, 31), JobDescription = "Hommikul atraktsiooni valmispanek. Päeval inimeste juhendamine, atraktsiooni eest hoolitsemine, korra pidamine, inimestega suhtlemine. Õhtul töökoha korda tegemine ja kokku panemine." });
                jobExperienceService.SaveJobExperience(new JobExperience() { PersonId = 1, Company = "OÜ Vudila Mängumaa", JobTitle = "Instruktor", StartDate = new DateTime(2014, 06, 01), EndDate = new DateTime(2014, 08, 31), JobDescription = "Hommikul atraktsiooni valmispanek. Päeval inimeste juhendamine, atraktsiooni eest hoolitsemine, korra pidamine, inimestega suhtlemine. Õhtul töökoha korda tegemine ja kokku panemine." });
                jobExperienceService.SaveJobExperience(new JobExperience() { PersonId = 1, Company = "AS Selver", JobTitle = "Kassapidaja", StartDate = new DateTime(2016, 06, 26), EndDate = new DateTime(2016, 08, 20), JobDescription = "Kassas kauba läbi löömine, saalis kauba väljapanek, ära võtmine ja korrastamine. Klientidega suhtlemine." });

            }
            if (languageSkillService.GetAllLanguageSkills().Count == 0)
            {
                languageSkillService.SaveLanguageSkill(new LanguageSkill() { PersonId = 1, Language = "Eesti", IsNativeLanguage = true, ReadingLevel = LanguageSkillLevel.C2, SpeakingLevel = LanguageSkillLevel.C2, WritingLevel = LanguageSkillLevel.C2, ListeningLevel = LanguageSkillLevel.C2 });
                languageSkillService.SaveLanguageSkill(new LanguageSkill() { PersonId = 1, Language = "Inglise", IsNativeLanguage = false, ReadingLevel = LanguageSkillLevel.B2, SpeakingLevel = LanguageSkillLevel.B2, WritingLevel = LanguageSkillLevel.C1, ListeningLevel = LanguageSkillLevel.B2 });
                languageSkillService.SaveLanguageSkill(new LanguageSkill() { PersonId = 1, Language = "Vene", IsNativeLanguage = false, ReadingLevel = LanguageSkillLevel.B1, SpeakingLevel = LanguageSkillLevel.B1, WritingLevel = LanguageSkillLevel.B1, ListeningLevel = LanguageSkillLevel.B1 });
            }
            if (sertificateService.GetAllSertificates().Count == 0)
            {
                sertificateService.SaveSertificate(new Sertificate() { PersonId = 1, SertificateName = "Cambridge English Level 1 Certificate in ESOL International", SertificateLevel = "B2", DateAcquired = new DateTime(2016, 02, 05), SertificatePath = "http://enos.itcollege.ee/~arasta/Sertificates/CAE_sertificate.pdf" });
            }
            if (skillService.GetAllSkills().Count == 0)
            {
                skillService.SaveSkill(new Skill() { PersonId = 1, SkillName = "C#", SkillLevel = SkillLevel.Edasijõudnud });
                skillService.SaveSkill(new Skill() { PersonId = 1, SkillName = "Java", SkillLevel = SkillLevel.Keskmine });
                skillService.SaveSkill(new Skill() { PersonId = 1, SkillName = "HTML", SkillLevel = SkillLevel.Keskmine });
                skillService.SaveSkill(new Skill() { PersonId = 1, SkillName = "JavaScript", SkillLevel = SkillLevel.Tavakasutaja });
                skillService.SaveSkill(new Skill() { PersonId = 1, SkillName = "Python", SkillLevel = SkillLevel.Algaja });
                skillService.SaveSkill(new Skill() { PersonId = 1, SkillName = "Git", SkillLevel = SkillLevel.Keskmine });
                skillService.SaveSkill(new Skill() { PersonId = 1, SkillName = "ERD", SkillLevel = SkillLevel.Keskmine });
                skillService.SaveSkill(new Skill() { PersonId = 1, SkillName = "MS Excel", SkillLevel = SkillLevel.Edasijõudnud });
                skillService.SaveSkill(new Skill() { PersonId = 1, SkillName = "MS Word", SkillLevel = SkillLevel.Edasijõudnud });
                skillService.SaveSkill(new Skill() { PersonId = 1, SkillName = "MS Visual Studio", SkillLevel = SkillLevel.Keskmine });
            }
            if (questionService.GetAllQuestions().Count == 0)
            {
                questionService.SaveQuestion(new Question() { QuestionContent = "Mis on CV omaniku nimi?", Hint = "Selle nimega on ka üks kollane lill ja ilutulestiku firma" });
                questionService.SaveQuestion(new Question() { QuestionContent = "Kus koolis sai Arnika keskhariduse?", Hint = "Kooli nimes on kuulsa Eesti kirjaniku nimi, kes kirjutas raamatu \"Kevade\"" });
                questionService.SaveQuestion(new Question() { QuestionContent = "Mis koolis Arnika omandab kõrgharidust?", Hint = "Kooli maskotiks on pingviin ja tunnusmuster oli mitut tooni siniseid kujundeid" });
                questionService.SaveQuestion(new Question() { QuestionContent = "Kui vana oli Arnika kui ta sai autojuhiload 12.09.2015?", Hint = "Sünnikuupäev on 12.09.1997" });
                questionService.SaveQuestion(new Question() { QuestionContent = "Mis kuupäeval on Arnika sünnipäev?", Hint = "Üks päev enne seda kuupäva toimus kaksiktornide õnnetus" });
                questionService.SaveQuestion(new Question() { QuestionContent = "Mis programmeerimiskeelt Arnika oskab kõige paremini", Hint = "Selles programmeerimiskeeles on märk mida noored tihti kasutavad tänapäeval" });
                questionService.SaveQuestion(new Question() { QuestionContent = "Mis kooke Arnika oskab küpsetada?", Hint = "Mõnikord võivad kõik vastusevariandid magusad olla..." });
            }
            if (answerService.GetAllAnswers().Count == 0)
            {
                answerService.SaveAnswer(new Answer() { QuestionId = 1, AnswerContent = "Annika", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 1, AnswerContent = "Anna-Liis", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 1, AnswerContent = "Arnika", IsRight = true });
                answerService.SaveAnswer(new Answer() { QuestionId = 1, AnswerContent = "Alice", IsRight = false });

                answerService.SaveAnswer(new Answer() { QuestionId = 2, AnswerContent = "Juhan Liivi nimeline Alatskivi Keskkool", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 2, AnswerContent = "Oskar Lutsu Palamuse Gümnaasium", IsRight = true });
                answerService.SaveAnswer(new Answer() { QuestionId = 2, AnswerContent = "August Kitzbergi nimeline Gümnaasium", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 2, AnswerContent = "Tartu Kristjan Jaak Petersoni Gümnaasium", IsRight = false });

                answerService.SaveAnswer(new Answer() { QuestionId = 3, AnswerContent = "Tallinna Ülikool", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 3, AnswerContent = "Infotehnoloogia Kolledž", IsRight = true });
                answerService.SaveAnswer(new Answer() { QuestionId = 3, AnswerContent = "Tartu Ülikool", IsRight = false });

                answerService.SaveAnswer(new Answer() { QuestionId = 4, AnswerContent = "17", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 4, AnswerContent = "18", IsRight = true });
                answerService.SaveAnswer(new Answer() { QuestionId = 4, AnswerContent = "19", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 4, AnswerContent = "20", IsRight = false });

                answerService.SaveAnswer(new Answer() { QuestionId = 5, AnswerContent = "25.03", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 5, AnswerContent = "07.08", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 5, AnswerContent = "12.09", IsRight = true });
                answerService.SaveAnswer(new Answer() { QuestionId = 5, AnswerContent = "11.10", IsRight = false });

                answerService.SaveAnswer(new Answer() { QuestionId = 6, AnswerContent = "C#", IsRight = true });
                answerService.SaveAnswer(new Answer() { QuestionId = 6, AnswerContent = "Java", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 6, AnswerContent = "Python", IsRight = false });
                answerService.SaveAnswer(new Answer() { QuestionId = 6, AnswerContent = "C++", IsRight = false });

                answerService.SaveAnswer(new Answer() { QuestionId = 7, AnswerContent = "Sidruni-beseekook", IsRight = true });
                answerService.SaveAnswer(new Answer() { QuestionId = 7, AnswerContent = "Dopsikook", IsRight = true });
                answerService.SaveAnswer(new Answer() { QuestionId = 7, AnswerContent = "Õuna või rabarberikook", IsRight = true });
                answerService.SaveAnswer(new Answer() { QuestionId = 7, AnswerContent = "Pavlova", IsRight = true });
            }

            Console.WriteLine("Finished");
            System.Console.ReadLine();
        }
    }
}
