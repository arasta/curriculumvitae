﻿using CurriculumVitae.Domains;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae
{
    public class CurriculumVitaeDbContext : DbContext
    {
        public DbSet<DrivingLicense> DrivingLicenses { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<JobExperience> JobExperiences { get; set; }
        public DbSet<LanguageSkill> LanguageSkills { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Sertificate> Sertificates { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public CurriculumVitaeDbContext() : base("name=ArnikaCurriculumVitaeDbSql")
        {
        }
    }
}
