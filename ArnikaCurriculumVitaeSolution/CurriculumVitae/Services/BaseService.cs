﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Services
{
    public class BaseService
    {
        protected CurriculumVitaeDbContext DataContext;

        public BaseService(CurriculumVitaeDbContext ctx)
        {
            DataContext = ctx ?? throw new Exception("Missing DatabaseContext");
        }
    }
}
