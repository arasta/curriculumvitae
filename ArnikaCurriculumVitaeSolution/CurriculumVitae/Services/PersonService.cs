﻿using CurriculumVitae.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurriculumVitae.Domains;

namespace CurriculumVitae.Services
{
    public class PersonService : BaseService, IPersonService
    {
        public PersonService(CurriculumVitaeDbContext dbContext) : base(dbContext) { }

        public List<Person> GetAllPersons()
        {
            return DataContext.People.ToList();
        }

        public Person GetPersonById(int id)
        {
            return DataContext.People.Where(p => p.PersonId == id).SingleOrDefault();
        }

        public Person SavePerson(Person newPerson)
        {
            var dbPerson = DataContext.People.Where(p => p.PersonId == newPerson.PersonId).FirstOrDefault();
            var isNew = dbPerson == null;

            var person = isNew ? new Person() : dbPerson;

            person.Firstname = newPerson.Firstname;
            person.Lastname = newPerson.Lastname;
            person.Gender = newPerson.Gender;
            person.BirthDate = newPerson.BirthDate;
            person.Address = newPerson.Address;
            person.Telephone = newPerson.Telephone;
            person.Email = newPerson.Email;

            if (isNew) DataContext.People.Add(person);

            DataContext.SaveChanges();
            return person;
        }
    }
}
