﻿using CurriculumVitae.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurriculumVitae.Domains;

namespace CurriculumVitae.Services
{
    public class AnswerService : BaseService, IAnswerService
    {
        public AnswerService(CurriculumVitaeDbContext dbContext) : base(dbContext) { }

        public List<Answer> GetAllAnswers()
        {
            return DataContext.Answers.ToList();
        }

        public List<Answer> GetAnswersByQuestionId(int qId)
        {
            return DataContext.Answers.Where(x => x.QuestionId == qId).ToList();
        }

        public Answer SaveAnswer(Answer newAnswer)
        {
            var dbAnswer = DataContext.Answers.Where(p => p.AnswerId == newAnswer.AnswerId).FirstOrDefault();
            var isNew = dbAnswer == null;

            var answer = isNew ? new Answer() : dbAnswer;

            answer.AnswerId = newAnswer.AnswerId;
            answer.AnswerContent = newAnswer.AnswerContent;
            answer.IsRight = newAnswer.IsRight;
            answer.QuestionId = newAnswer.QuestionId;

            if (isNew) DataContext.Answers.Add(answer);

            DataContext.SaveChanges();
            return answer;
        }
    }
}
