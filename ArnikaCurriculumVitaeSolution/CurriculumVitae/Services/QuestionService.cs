﻿using CurriculumVitae.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurriculumVitae.Domains;

namespace CurriculumVitae.Services
{
    public class QuestionService : BaseService, IQuestionService
    {
        public QuestionService(CurriculumVitaeDbContext dbContext) : base(dbContext) { }

        public List<Question> GetAllQuestions()
        {
            return DataContext.Questions.OrderBy(x => x.QuestionId).ToList();
        }

        public Question SaveQuestion(Question newQuestion)
        {
            var dbQuestion = DataContext.Questions.Where(p => p.QuestionId == newQuestion.QuestionId).FirstOrDefault();
            var isNew = dbQuestion == null;

            var question = isNew ? new Question() : dbQuestion;

            question.QuestionId = newQuestion.QuestionId;
            question.QuestionContent = newQuestion.QuestionContent;
            question.Hint = newQuestion.Hint;

            if (isNew) DataContext.Questions.Add(question);

            DataContext.SaveChanges();
            return question;
        }
    }
}
