﻿using CurriculumVitae.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurriculumVitae.Domains;

namespace CurriculumVitae.Services
{
    class SkillService : BaseService, ISkillService
    {
        public SkillService(CurriculumVitaeDbContext dbContext) : base(dbContext) { }

        public List<Skill> GetAllSkills()
        {
            return DataContext.Skills.ToList();
        }

        public Skill SaveSkill(Skill newSkill)
        {
            var dbSkill = DataContext.Skills.Where(p => p.SkillId == newSkill.SkillId).FirstOrDefault();
            var isNew = dbSkill == null;

            var skill = isNew ? new Skill() : dbSkill;

            skill.PersonId = newSkill.PersonId;
            skill.SkillName = newSkill.SkillName;
            skill.SkillLevel = newSkill.SkillLevel;

            if (isNew) DataContext.Skills.Add(skill);

            DataContext.SaveChanges();
            return skill;
        }
    }
}
