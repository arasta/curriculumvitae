﻿using CurriculumVitae.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurriculumVitae.Domains;

namespace CurriculumVitae.Services
{
    public class EducationService : BaseService, IEducationService
    {
        public EducationService(CurriculumVitaeDbContext dbContext) : base(dbContext) { }

        public List<Education> GetAllEducations()
        {
            return DataContext.Educations.ToList();
        }

        public Education SaveEducation(Education newEducation)
        {
            var dbEducation = DataContext.Educations.Where(p => p.EducationId == newEducation.EducationId).FirstOrDefault();
            var isNew = dbEducation == null;

            var education = isNew ? new Education() : dbEducation;

            education.PersonId = newEducation.PersonId;
            education.SchoolName = newEducation.SchoolName;
            education.LevelOfEducation = newEducation.LevelOfEducation;
            education.StartYear = newEducation.StartYear;
            education.Graduated = newEducation.Graduated;
            education.EndYear = newEducation.EndYear;

            if (isNew) DataContext.Educations.Add(education);

            DataContext.SaveChanges();
            return education;
        }
    }
}
