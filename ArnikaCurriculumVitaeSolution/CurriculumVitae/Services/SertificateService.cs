﻿using CurriculumVitae.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurriculumVitae.Domains;

namespace CurriculumVitae.Services
{
    class SertificateService : BaseService, ISertificateService
    {
        public SertificateService(CurriculumVitaeDbContext dbContext) : base(dbContext) { }

        public List<Sertificate> GetAllSertificates()
        {
            return DataContext.Sertificates.ToList();
        }

        public Sertificate SaveSertificate(Sertificate newSertificate)
        {
            var dbSertificate = DataContext.Sertificates.Where(p => p.SertificateId == newSertificate.SertificateId).FirstOrDefault();
            var isNew = dbSertificate == null;

            var sertificate = isNew ? new Sertificate() : dbSertificate;

            sertificate.PersonId = newSertificate.PersonId;
            sertificate.SertificateName = newSertificate.SertificateName;
            sertificate.SertificateLevel = newSertificate.SertificateLevel;
            sertificate.DateAcquired = newSertificate.DateAcquired;
            sertificate.SertificatePath = newSertificate.SertificatePath;

            if (isNew) DataContext.Sertificates.Add(sertificate);

            DataContext.SaveChanges();
            return sertificate;
        }
    }
}
