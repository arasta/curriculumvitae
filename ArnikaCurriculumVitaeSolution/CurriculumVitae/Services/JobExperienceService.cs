﻿using CurriculumVitae.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurriculumVitae.Domains;

namespace CurriculumVitae.Services
{
    class JobExperienceService : BaseService, IJobExperienceService
    {
        public JobExperienceService(CurriculumVitaeDbContext dbContext) : base(dbContext) { }

        public List<JobExperience> GetAllJobExperiences()
        {
            return DataContext.JobExperiences.ToList();
        }

        public JobExperience SaveJobExperience(JobExperience newJobExperience)
        {
            var dbJobExperience = DataContext.JobExperiences.Where(p => p.JobExperienceId == newJobExperience.JobExperienceId).FirstOrDefault();
            var isNew = dbJobExperience == null;

            var jobExperience = isNew ? new JobExperience() : dbJobExperience;

            jobExperience.PersonId = newJobExperience.PersonId;
            jobExperience.JobTitle = newJobExperience.JobTitle;
            jobExperience.Company = newJobExperience.Company;
            jobExperience.StartDate = newJobExperience.StartDate;
            jobExperience.EndDate = newJobExperience.EndDate;
            jobExperience.JobDescription = newJobExperience.JobDescription;

            if (isNew) DataContext.JobExperiences.Add(jobExperience);

            DataContext.SaveChanges();
            return jobExperience;
        }
    }
}
