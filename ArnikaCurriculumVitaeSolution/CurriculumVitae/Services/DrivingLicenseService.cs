﻿using CurriculumVitae.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurriculumVitae.Domains;

namespace CurriculumVitae.Services
{
    public class DrivingLicenseService : BaseService, IDrivingLicenseService
    {
        public DrivingLicenseService(CurriculumVitaeDbContext dbContext) : base(dbContext) { }

        public List<DrivingLicense> GetAllDrivinglicenses()
        {
            return DataContext.DrivingLicenses.ToList();
        }

        public DrivingLicense SaveDrivinglicense(DrivingLicense newDrivingLicense)
        {
            var dbDrivingLicense = DataContext.DrivingLicenses.Where(p => p.DrivingLicenseId == newDrivingLicense.DrivingLicenseId).FirstOrDefault();
            var isNew = dbDrivingLicense == null;

            var drivingLicense = isNew ? new DrivingLicense() : dbDrivingLicense;

            drivingLicense.PersonId = newDrivingLicense.PersonId;
            drivingLicense.DrivingLicenseCategory = newDrivingLicense.DrivingLicenseCategory;
            drivingLicense.DateAcquired = newDrivingLicense.DateAcquired;

            if (isNew) DataContext.DrivingLicenses.Add(drivingLicense);

            DataContext.SaveChanges();
            return drivingLicense;
        }
    }
}
