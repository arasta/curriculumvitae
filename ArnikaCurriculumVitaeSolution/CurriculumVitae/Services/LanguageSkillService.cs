﻿using CurriculumVitae.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurriculumVitae.Domains;

namespace CurriculumVitae.Services
{
    class LanguageSkillService : BaseService, ILanguageSkillService
    {
        public LanguageSkillService(CurriculumVitaeDbContext dbContext) : base(dbContext) { }

        public List<LanguageSkill> GetAllLanguageSkills()
        {
            return DataContext.LanguageSkills.ToList();
        }

        public LanguageSkill SaveLanguageSkill(LanguageSkill newLanguageSkill)
        {
            var dbLanguageSkill = DataContext.LanguageSkills.Where(p => p.LanguageSkillId == newLanguageSkill.LanguageSkillId).FirstOrDefault();
            var isNew = dbLanguageSkill == null;

            var languageSkill = isNew ? new LanguageSkill() : dbLanguageSkill;

            languageSkill.PersonId = newLanguageSkill.PersonId;
            languageSkill.IsNativeLanguage = newLanguageSkill.IsNativeLanguage;
            languageSkill.Language = newLanguageSkill.Language;
            languageSkill.WritingLevel = newLanguageSkill.WritingLevel;
            languageSkill.SpeakingLevel = newLanguageSkill.SpeakingLevel;
            languageSkill.ReadingLevel = newLanguageSkill.ReadingLevel;
            languageSkill.ListeningLevel = newLanguageSkill.ListeningLevel;

            if (isNew) DataContext.LanguageSkills.Add(languageSkill);

            DataContext.SaveChanges();
            return languageSkill;
        }
    }
}
