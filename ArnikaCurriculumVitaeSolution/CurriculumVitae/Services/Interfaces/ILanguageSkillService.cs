﻿using CurriculumVitae.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Services.Interfaces
{
    public interface ILanguageSkillService
    {
        LanguageSkill SaveLanguageSkill(LanguageSkill newLanguageSkill);
        List<LanguageSkill> GetAllLanguageSkills();
    }
}
