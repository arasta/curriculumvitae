﻿using CurriculumVitae.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Services.Interfaces
{
    public interface ISkillService
    {
        Skill SaveSkill(Skill newSkill);
        List<Skill> GetAllSkills();
    }
}
