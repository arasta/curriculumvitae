﻿using CurriculumVitae.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Services.Interfaces
{
    public interface IJobExperienceService
    {
        JobExperience SaveJobExperience(JobExperience newJobExperience);
        List<JobExperience> GetAllJobExperiences();
    }
}
