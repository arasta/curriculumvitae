﻿using CurriculumVitae.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Services.Interfaces
{
    public interface IPersonService
    {
        Person SavePerson(Person newPerson);
        List<Person> GetAllPersons();
        Person GetPersonById(int id);
    }
}
