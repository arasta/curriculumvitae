﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains
{
    public class Question
    {
        public int QuestionId { get; set; }
        [MaxLength(300)]
        public string QuestionContent { get; set; }
        [MaxLength(350)]
        public string Hint { get; set; }

        public virtual List<Answer> Answers { get; set; } = new List<Answer>();

    }
}
