﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains
{
    public class Answer
    {
        public int AnswerId { get; set; }
        [MaxLength(300)]
        public string AnswerContent { get; set; }
        public bool IsRight { get; set; }

        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
    }
}
