﻿using CurriculumVitae.Domains.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains
{
    public class LanguageSkill
    {
        public int LanguageSkillId { get; set; }
        [MaxLength(100)]
        public string Language { get; set; }
        public LanguageSkillLevel WritingLevel { get; set; }
        public LanguageSkillLevel SpeakingLevel { get; set; }
        public LanguageSkillLevel ListeningLevel { get; set; }
        public LanguageSkillLevel ReadingLevel { get; set; }
        public bool IsNativeLanguage { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}
