﻿using CurriculumVitae.Domains.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains
{
    public class Skill
    {
        public int SkillId { get; set; }
        [MaxLength(150)]
        public string SkillName { get; set; }
        public SkillLevel SkillLevel { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}
