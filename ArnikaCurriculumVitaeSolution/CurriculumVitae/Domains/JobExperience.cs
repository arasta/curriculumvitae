﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains
{
    public class JobExperience
    {
        public int JobExperienceId { get; set; }
        [MaxLength(200)]
        public string Company { get; set; }
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; } = new DateTime(2999, 01, 01);
        [MaxLength(100)]
        public string JobTitle { get; set; }
        [MaxLength(350)]
        public string JobDescription { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }

    }
}
