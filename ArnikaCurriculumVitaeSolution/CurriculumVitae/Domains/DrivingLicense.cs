﻿using CurriculumVitae.Domains.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains
{
    public class DrivingLicense
    {
        public int DrivingLicenseId { get; set; }
        public DrivingLicenseCategory DrivingLicenseCategory { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateAcquired { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}
