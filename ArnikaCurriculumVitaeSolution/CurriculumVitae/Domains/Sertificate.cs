﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains
{
    public class Sertificate
    {
        public int SertificateId { get; set; }
        [MaxLength(200)]
        public string SertificateName { get; set; }
        [MaxLength(75)]
        public string SertificateLevel { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateAcquired { get; set; }
        [MaxLength(200)]
        public string SertificatePath { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}
