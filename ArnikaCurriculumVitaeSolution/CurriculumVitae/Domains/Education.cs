﻿using CurriculumVitae.Domains.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains
{
    public class Education
    {
        public int EducationId { get; set; }
        [MaxLength(200)]
        public string SchoolName { get; set; }
        public int StartYear { get; set; }
        public int? EndYear { get; set; }
        public bool Graduated { get; set; }
        public LevelOfEducation LevelOfEducation { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}
