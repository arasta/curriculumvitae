﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains.Enums
{
    public enum DrivingLicenseCategory
    {
        AM,
        A1,
        A2,
        A,
        B1,
        B,
        C1,
        C,
        D1,
        D,
        BE,
        C1E,
        CE,
        D1E,
        DE
    }
}
