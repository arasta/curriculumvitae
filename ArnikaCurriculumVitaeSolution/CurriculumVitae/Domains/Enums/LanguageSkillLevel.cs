﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains.Enums
{
    public enum LanguageSkillLevel
    {
        A1,
        A2,
        B1,
        B2,
        C1,
        C2
    }
}
