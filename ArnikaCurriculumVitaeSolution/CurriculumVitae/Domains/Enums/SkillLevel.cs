﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains.Enums
{
    public enum SkillLevel
    {
        Algaja = 1,
        Tavakasutaja = 2,
        Keskmine = 3,
        Edasijõudnud = 4,
        Spetsialist = 5
    }
}
