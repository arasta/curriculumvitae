﻿using CurriculumVitae.Domains.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.Domains
{
    public class Person
    {
        public int PersonId { get; set; }
        [MaxLength(75)]
        public string Firstname { get; set; }
        [MaxLength(75)]
        public string Lastname { get; set; }
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }
        [MaxLength(50)]
        public string Telephone { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        [MaxLength(200)]
        public string Address { get; set; }

        public Gender Gender { get; set; }

        public virtual List<JobExperience> PersonJobExperiences { get; set; } = new List<JobExperience>();
        public virtual List<LanguageSkill> PersonLanguageSkills { get; set; } = new List<LanguageSkill>();
        public virtual List<Education> PersonEducations { get; set; } = new List<Education>();
        public virtual List<Skill> PersonSkills { get; set; } = new List<Skill>();
        public virtual List<DrivingLicense> PersonDrivingLicenses { get; set; } = new List<DrivingLicense>();
        public virtual List<Sertificate> PersonSertificates { get; set; } = new List<Sertificate>();

        public string FullName
        {
            get
            {
                return Firstname + " " + Lastname;
            }
        }
    }
}