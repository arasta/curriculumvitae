﻿using CurriculumVitae.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CurriculumVitae.WPF.Views
{
    /// <summary>
    /// Interaction logic for QuizView.xaml
    /// </summary>
    public partial class QuizView : UserControl
    {
        private QuizVM _vm;
        public QuizView()
        {
            InitializeComponent();
            this.Loaded += Quiz_Loaded;
        }

        private void Quiz_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new QuizVM();

            this.DataContext = _vm;
        }

        private void btnSelectedAnswer_Click(object sender, RoutedEventArgs e)
        {
            int res;
            var btn = sender as Button;
            bool isRigth = (bool)btn.Tag;
            if (isRigth)
            {
                MessageBox.Show("Vastasid õigesti!");
                _vm.IncreasePoints();
                res = _vm.NextQuestion();

            }
            else
            {
                MessageBox.Show("Vale vastus. Õige vastus oli: " + _vm.CurrentQuestionAnswers.Where(x => x.IsRight == true).Single().AnswerContent);
                res = _vm.NextQuestion();
            }
            if (res == 1)
            {
                MessageBox.Show("Jõudsid küsimustiku lõppu. Vastasid õigesti " + _vm.QuestionPoints + " küsimusele.");
                _vm.ResetQuestion();
            }
        }
    }
}
