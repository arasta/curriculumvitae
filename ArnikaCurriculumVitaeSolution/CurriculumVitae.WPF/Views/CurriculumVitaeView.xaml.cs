﻿using CurriculumVitae.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CurriculumVitae.WPF.Views
{
    /// <summary>
    /// Interaction logic for CurriculumVitaeView.xaml
    /// </summary>
    public partial class CurriculumVitaeView : UserControl
    {
        private CurriculumVitaeVM _vm;
        public CurriculumVitaeView()
        {
            InitializeComponent();
            this.Loaded += CurriculumVitae_Loaded;
        }

        private void CurriculumVitae_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new CurriculumVitaeVM();

            this.DataContext = _vm;
        }

        private void Sertificate_Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink link = (Hyperlink)e.OriginalSource;
            Process.Start(link.NavigateUri.AbsoluteUri);
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }
    }
}
