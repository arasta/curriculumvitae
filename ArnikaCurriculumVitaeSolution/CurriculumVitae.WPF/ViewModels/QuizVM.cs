﻿using CurriculumVitae.Domains;
using CurriculumVitae.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.WPF.ViewModels
{
    class QuizVM : BaseVM
    {
        private Question _selectedQuestion;
        private List<Question> _questions;
        private QuestionService _questionService;
        private int _questionPoints = 0;
        private int _questionId = 0;

        private List<Answer> _answers;
        private AnswerService _answerService;
        private CurriculumVitaeDbContext _db;

        public QuizVM()
        {
            _db = new CurriculumVitaeDbContext();
            _questionService = new QuestionService(_db);
            _answerService = new AnswerService(_db);
            _questions = _questionService.GetAllQuestions();
            _selectedQuestion = _questions[_questionId];
            _answers = _answerService.GetAnswersByQuestionId(_selectedQuestion.QuestionId);
        }

        public int QuestionPoints
        {
            get => _questionPoints;
            set
            {
                _questionPoints = value;
                NotifyPropertyChanged("QuestionPoints");
            }
        }

        internal void IncreasePoints()
        {
            QuestionPoints = QuestionPoints + 1;
        }

        internal int NextQuestion()
        {
            _questionId = _questionId + 1;
            if (_questionId < _questions.Count())
            {
                CurrentQuestion = _questions[_questionId];
                CurrentQuestionAnswers = _answerService.GetAnswersByQuestionId(_selectedQuestion.QuestionId);
                return 0;
            }
            else
            {
                return 1;
            }


        }

        internal void ResetQuestion()
        {
            QuestionPoints = 0;
            CurrentQuestion = _questions[0];
            CurrentQuestionAnswers = _answerService.GetAnswersByQuestionId(_selectedQuestion.QuestionId);
        }

        public Question CurrentQuestion
        {
            get => _selectedQuestion;
            set
            {
                _selectedQuestion = value;
                NotifyPropertyChanged("CurrentQuestion");
            }
        }

        public List<Answer> CurrentQuestionAnswers
        {
            get => _answers;
            set
            {
                _answers = value;
                NotifyPropertyChanged("CurrentQuestionAnswers");

            }
        }

    }
}
