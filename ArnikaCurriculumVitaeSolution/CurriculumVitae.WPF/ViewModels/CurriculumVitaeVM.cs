﻿using CurriculumVitae.Domains;
using CurriculumVitae.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.WPF.ViewModels
{
    public class CurriculumVitaeVM : BaseVM
    {
        private Person _person;
        private PersonService _personService;
        private CurriculumVitaeDbContext _db;

        public CurriculumVitaeVM()
        {
            _db = new CurriculumVitaeDbContext();
            _personService = new PersonService(_db);
            _person = _personService.GetPersonById(1);
        }

        public Person CurrentPerson => _person;
    }
}
