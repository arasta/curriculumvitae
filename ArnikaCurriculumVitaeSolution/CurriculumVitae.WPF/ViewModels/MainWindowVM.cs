﻿using CurriculumVitae.Domains;
using CurriculumVitae.Services.Interfaces;
using CurriculumVitae.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurriculumVitae.WPF.ViewModels
{
    public class MainWindowVM : BaseVM
    {
        private object _content;

        public MainWindowVM()
        {
            _content = new CurriculumVitaeVM();
        }
        public object Content
        {
            get { return _content; }
            set
            {
                _content = value;
                NotifyPropertyChanged("Content");
            }
        }
    }
}
