﻿using CurriculumVitae.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CurriculumVitae.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowVM _vm;
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new MainWindowVM();
            this.DataContext = _vm;
        }

        private void btnGoToQuiz_Click(object sender, RoutedEventArgs e)
        {
            _vm.Content = new QuizVM();
        }

        private void btnGoToCV_Click(object sender, RoutedEventArgs e)
        {
            _vm.Content = new CurriculumVitaeVM();
        }
    }
}
