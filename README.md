## Clone git repository to open project

1. git clone https://arasta@bitbucket.org/arasta/curriculumvitae.git
2. Open ArnikaCurriculumVitaeSolution.sln in folder ArnikaCurriculumVitaeSolution

---

## Fill database

Database is filled with data from Program.cs

1. Set CurriculumVitae as StartUp Project
2. Start Curriculum Vitae
3. When console says Finished then the database is filled and console can be closed

---

## Open CV

1. Set CurriculumVitae.WPF as StartUp Project
2. Start CurriculumVitae.WPF
3. Read CV